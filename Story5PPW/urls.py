
from django.contrib import admin
from django.urls import include, path, re_path
from Lab5PPW.views import *

urlpatterns = [
    path('admin/', admin.site.urls),
    path('lab6', index, name = "index"),
    path('lab6/getbooks', getBooks, name="getbooks"),
	path('lab6/login', login, name="login"),
    re_path(r'^profile', profile, name = "profile"),
    re_path(r'^index', index, name = "index")
]
