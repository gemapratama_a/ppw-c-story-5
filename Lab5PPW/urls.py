from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('profile', profile, name = "profile"),
    path('getbooks', getBooks, name="getBooks")
    ]