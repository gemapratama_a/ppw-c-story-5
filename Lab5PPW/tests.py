from django.test import TestCase
from django.test import Client
from django.urls import resolve
from Lab5PPW.views import *
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from django.test import TestCase
from selenium.webdriver.common.by import By
from django.contrib.staticfiles.storage import staticfiles_storage
from django.contrib.staticfiles import finders
import unittest
import time
import requests
# Inspirasi-inspirasi dari:
# https://gitlab.com/PPW-2017/ppw-lab/blob/master/lab_instruction/lab_5/README.md
# https://www.obeythetestinggoat.com/book/chapter_prettification.html
# https://stackoverflow.com/questions/22991071/django-test-loading-css-file

class Lab9UnitTest(TestCase):

    def test_landing_page_exists(self):
        response = Client().get('/lab6')
        self.assertEqual(response.status_code, 200)

    def test_lab9_using_index_func(self):
        found = resolve('/lab6')
        self.assertEqual(found.func, index)

    def test_get_books_list(self):
        response = Client().get('/lab6/getbooks')
        self.assertEqual(response.status_code, 200)

    def test_landing_page_using_index_template(self):
        response = Client().get('/lab6')
        self.assertTemplateUsed(response, 'Lab5PPW/index.html')
    
