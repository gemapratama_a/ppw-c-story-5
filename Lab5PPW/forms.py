from django import forms
from Lab5PPW.models import Status
#done
class Form(forms.ModelForm):
    status = forms.CharField(max_length = 300)

    class Meta:
        model = Status
        fields = ('status',)
