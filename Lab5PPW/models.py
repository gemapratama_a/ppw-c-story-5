from django.db import models

class Book(models.Model): # pragma: no cover
    title = models.CharField(max_length = 500)
    description = models.CharField(max_length = 1000)
    authors = models.CharField(max_length = 500)
    published_date = models.CharField(max_length = 500)
    page_count = models.IntegerField(default =0 )
    categories = models.CharField(max_length = 500)
    
    def __str__(self): # pragma: no cover
        return self.title
