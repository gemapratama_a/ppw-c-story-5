# Generated by Django 2.1.2 on 2018-11-20 10:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Lab5PPW', '0008_visitor_password'),
    ]

    operations = [
        migrations.CreateModel(
            name='Subscriber',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=300)),
                ('email', models.EmailField(max_length=300)),
                ('password', models.CharField(default='', max_length=300)),
            ],
        ),
        migrations.DeleteModel(
            name='Visitor',
        ),
    ]
