# Generated by Django 2.1.2 on 2018-11-18 08:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Lab5PPW', '0006_auto_20181111_1313'),
    ]

    operations = [
        migrations.CreateModel(
            name='Visitor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=300)),
                ('email', models.CharField(max_length=300)),
            ],
        ),
        migrations.DeleteModel(
            name='Book',
        ),
    ]
