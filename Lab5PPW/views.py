import os
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
import requests
import json
# https://www.youtube.com/watch?v=g4wdm488mkE
# https://github.com/sam-suresh/JSON-URL-to-HTML-Table

def profile(request):
    return render(request, 'Lab5PPW/profile.html', {})

def index(request):
    return render(request, 'Lab5PPW/index.html', {})
	
def login(request):
    return render(request, 'Lab5PPW/login.html', {})

def getBooks(request):
    default_search_url = 'https://www.googleapis.com/books/v1/volumes?q=' # default
    
    if request.method == 'POST':
        keyword = request.POST['query']
        default_search_url += keyword
    else:
        default_search_url += 'quilting'

    request = requests.get(default_search_url)
    json_data = request.json()

    args = {
        'books' : [],
    }
    for book in json_data['items']:
        book_data = {
            'title' : '',
            'description' : '',
            'authors' : [],
            'publishedDate' : '',
            'pageCount' : '',
            'categories' : []
        }
        if 'title' in book['volumeInfo']:
            book_data['title'] = book['volumeInfo']['title']
        if 'description' in book['volumeInfo']:
            book_data['description'] = book['volumeInfo']['description']
        if 'authors' in book['volumeInfo']:
            book_data['authors'] = [author for author in book['volumeInfo']['authors']]
        if 'publishedDate' in book['volumeInfo']:
            book_data['publishedDate'] = book['volumeInfo']['publishedDate']
        if 'pageCount' in book['volumeInfo']:
             int_value = book['volumeInfo']['pageCount']
             book_data['pageCount'] = str(int_value)
        if 'categories' in book['volumeInfo']:
            book_data['categories'] = [category for category in book['volumeInfo']['categories']]
        
        args['books'].append(book_data)

    return JsonResponse(args)