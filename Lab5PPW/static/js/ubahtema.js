function changetheme() {

    if (document.getElementById("changetheme").getAttribute("value") == "Ubah Tema: Gelap") {
        document.getElementById("changetheme").setAttribute("value", "Ubah Tema: Cerah");
        document.getElementById("pagestyle").setAttribute("href", theme1);
    } else {
        document.getElementById("changetheme").setAttribute("value", "Ubah Tema: Gelap");
        document.getElementById("pagestyle").setAttribute("href", theme2);
    }

}