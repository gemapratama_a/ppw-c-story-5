// https://www.googleapis.com/books/v1/volumes?q=quiltingtest
// https://www.youtube.com/watch?v=rJesac0_Ftw
// http://jsfiddle.net/manishmmulani/7MRx6
// https://www.youtube.com/watch?v=AOfSuajwY-I
// https://github.com/sam-suresh/JSON-URL-to-HTML-Table

$(document).ready(function () {
    var numOfFavBooks = 0;
    $.ajax({
        method: "GET",
        //url: " http://127.0.0.1:8000/lab6/getbooks",
		url: "https://ppwstory5.herokuapp.com/lab6/getbooks",
        success: function (result) {
            // console.log(result)
            var arrayOfBooks = result.books;
            for (var i = 0; i < arrayOfBooks.length; i++) {
                var button = '<button class="favbutton" img src="https://i.imgur.com/RiQWiom.png"><i class="favstar"></i></button>'
                var toAdd = 
                '<tr>' + 
                '<td>' + arrayOfBooks[i].title + '</td>' + 
                '<td>' + arrayOfBooks[i].description + '</td>' +
                '<td>' + arrayOfBooks[i].authors + '</td>' + 
                '<td>' + arrayOfBooks[i].publishedDate + '</td>' + 
                '<td>' + arrayOfBooks[i].pageCount + '</td>' + 
                '<td>' + arrayOfBooks[i].categories + '</td>' + 
                '<td>' + button + '</td>' + 
                '</tr>';
                $('tbody').append(toAdd);
            }
            $(".favbutton").click(function() {
                if ($(this).hasClass("selected")) {
                    $(this).find(".favstar").css("content", "url('../static/images/off.png')");
                    $(this).removeClass("selected");
                    numOfFavBooks--;
                    $("#numOfFavBooks").html(numOfFavBooks);
                } else {
                    $(this).find(".favstar").css("content", "url('../static/images/on.png')");
                    $(this).addClass("selected");
                    numOfFavBooks++;
                    $("#numOfFavBooks").html(numOfFavBooks);
                }
            });
        },
        error: function(error) {
            console.log("Error!")
        }
    });
});


$('.add-todo').click(function() {
    console.log('am i called');
      $.ajax({
          type: "POST",
          //url: "/getbooks",
          url: 'https://ppwstory5.herokuapp.com/lab6/getbooks',
          dataType: "json",
          data: {"item": $(".todo-item").val() },
          success: function(data) {
              alert(data.message);
          }
      });

  });